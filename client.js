function insertString(target, index, string) {
  if (index > 0) {
    return (
      target.substring(0, index) +
      string +
      target.substring(index, target.length)
    );
  }
  return string + target;
}

/**
 * @param {string} figure formatted string value eg: "1,024"
 * @returns {string}
 */
function parseAndConvert(figure, factor) {
  let expandedFigure = Number(figure.replaceAll(",", "")) * factor;
  let usd = expandedFigure / rate;
  let formattedUsd = Intl.NumberFormat("en", {
    style: "currency",
    currency: "USD",
    notation: "compact",
  }).format(usd);

  return formattedUsd;
}

/**
 * @param {string} str target string
 * @param {string} whole whole currency eg: "Tk 1,024 crore"
 */
function insertUsd(str, whole, converted) {
  let start = str.indexOf(whole);
  let len = whole.length;
  let end = start + len;

  return insertString(str, end, " (" + converted + ")");
}

const rate = 120; // rough value; 2024

window.addEventListener("load", () => {
  const text = document.querySelectorAll(
    "h1, h2, h3, h4, h5, p, li, td, caption, span, a",
  );
  for (let i = 0; i < text.length; i++) {
    let str = text[i].innerHTML;
    let croreMatches = str.matchAll(/Tk.? ?(.+?) crore/g);
    for (let match of croreMatches) {
      let converted = parseAndConvert(match[1], 1_00_00_000);
      str = insertUsd(str, match[0], converted);
      text[i].innerHTML = str;
    }
    let lakhMatches = str.matchAll(/Tk.? ?(.+?) lakh/g);
    for (let match of lakhMatches) {
      console.log(match[1])

      let converted = parseAndConvert(match[1], 1_00_000);
      str = insertUsd(str, match[0], converted);
      text[i].innerHTML = str;
    }
  }
});
